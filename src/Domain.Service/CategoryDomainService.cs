﻿
using Domain.Entities;
using Domain.IService;

using Infrastructure.IRepository;

namespace Domain.Service
{
    public class CategoryDomainService : BaseDomainService<Category>, ICategoryDomainService
    {
        public CategoryDomainService(IBaseRepository<Category> categoryBaseRepository) : base(categoryBaseRepository)
        {

        }
    }
}
