﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

using Domain.Entities;
using Domain.IService;

using Infrastructure.IRepository;

namespace Domain.Service
{
    public class BaseDomainService<T> : IBaseDomainService<T> where T : BaseEntity
    {
        protected readonly IBaseRepository<T> baseRepository;

        public BaseDomainService(IBaseRepository<T> baseRepository)
        {
            this.baseRepository = baseRepository;
        }

        public async Task<T> Delete(T entity)
        {
            return await baseRepository.Delete(entity);
        }

        public async Task<T> Find(Expression<Func<T, bool>> expression)
        {
            return await baseRepository.Find(expression);
        }

        public async Task<ICollection<T>> FindAll(params Expression<Func<T, object>>[] includes)
        {
            return await baseRepository.FindAll(includes);
        }

        public async Task<ICollection<T>> FindMany(Expression<Func<T, bool>> expression)
        {
            return await baseRepository.FindMany(expression);
        }

        public async Task<T> Save(T entity)
        {
            return await baseRepository.Save(entity);
        }

        public async Task<T> Update(T entity)
        {
            return await baseRepository.Update(entity);
        }
    }
}
