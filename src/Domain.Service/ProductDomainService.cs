﻿using System.Collections.Generic;
using System.Threading.Tasks;

using Domain.Entities;
using Domain.IService;

using Infrastructure.IRepository;

namespace Domain.Service
{
    public class ProductDomainService : BaseDomainService<Product>, IProductDomainService
    {
        private readonly IProductRepository productRepository;
        public ProductDomainService(IBaseRepository<Product> baseRepository, IProductRepository productRepository) : base(baseRepository)
        {
            this.productRepository = productRepository;
        }

        public async Task<ICollection<Product>> FindAllProducts()
        {
            return await productRepository.FindAllProducts();
        }

        public async Task<Product> FindProductByName(string name)
        {
            return await productRepository.FindProductByName(name);
        }
    }
}
