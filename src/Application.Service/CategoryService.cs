﻿using System.Collections.Generic;
using System.Threading.Tasks;

using Application.Entities;
using Application.IService;
using Application.Service.Exceptions;

using AutoMapper;

using Domain.Entities;
using Domain.IService;

namespace Application.Service
{
    public class CategoryService : ICategoryService
    {
        private readonly ICategoryDomainService categoryDomainService;
        private readonly IMapper mapper;

        public CategoryService(ICategoryDomainService categoryDomainService, IMapper mapper)
        {
            this.categoryDomainService = categoryDomainService;
            this.mapper = mapper;
        }

        public async Task<CategoryDTO> AddCategory(CategoryDTO catDTO)
        {
            var catInDb = await categoryDomainService.Find(c => c.Name == catDTO.Name);
            if (catInDb != null)
            {
                throw new DuplicateRecordInDbException("Theres already a category saved in Db");
            }
            return mapper.Map<CategoryDTO>(await categoryDomainService.Save(mapper.Map<Category>(catDTO)));
        }

        public async Task<CategoryDTO> DeleteCategory(CategoryDTO catDTO)
        {
            var catInDb = await categoryDomainService.Find(c => c.Name == catDTO.Name);
            if (catInDb == null)
            {
                throw new NoRecordFoundInDbException("");
            }
            return mapper.Map<CategoryDTO>(await categoryDomainService.Delete(catInDb));
        }

        public async Task<ICollection<CategoryDTO>> GetAllCategories()
        {
            return mapper.Map<List<CategoryDTO>>(await categoryDomainService.FindAll());
        }

        public async Task<CategoryDTO> GetCategoryByName(string name)
        {
            return mapper.Map<CategoryDTO>(await categoryDomainService.Find(c => c.Name == name));
        }

        public async Task<CategoryDTO> UpdateCategory(UpdateCategoryDTO updCatDTO)
        {
            var catInDb = await categoryDomainService.Find(p => p.Name == updCatDTO.OldName);
            if (catInDb == null)
            {
                throw new NoRecordFoundInDbException("");
            }
            catInDb.Name = updCatDTO.NewName;
            return mapper.Map<CategoryDTO>(await categoryDomainService.Update(catInDb));
        }
    }
}
