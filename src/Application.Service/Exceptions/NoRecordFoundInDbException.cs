﻿using System;

namespace Application.Service.Exceptions
{
    public class NoRecordFoundInDbException : ArgumentException
    {
        public NoRecordFoundInDbException(string message) : base(message)
        {
        }

        public NoRecordFoundInDbException(string message, Exception innerException) : base(message, innerException)
        {
        }

        public NoRecordFoundInDbException(string message, string paramName) : base(message, paramName)
        {
        }
    }
}
