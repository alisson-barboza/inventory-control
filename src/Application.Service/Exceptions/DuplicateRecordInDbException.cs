﻿using System;

namespace Application.Service.Exceptions
{
    public class DuplicateRecordInDbException : ArgumentException
    {
        public DuplicateRecordInDbException(string message) : base(message)
        {
        }

        public DuplicateRecordInDbException(string message, Exception innerException) : base(message, innerException)
        {
        }

        public DuplicateRecordInDbException(string message, string paramName) : base(message, paramName)
        {
        }

        public DuplicateRecordInDbException(string message, string paramName, Exception innerException) : base(message, paramName, innerException)
        {
        }
    }
}
