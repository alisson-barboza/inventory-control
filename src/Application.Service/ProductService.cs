﻿using System.Collections.Generic;
using System.Threading.Tasks;

using Application.Entities;
using Application.IService;
using Application.Service.Exceptions;

using AutoMapper;

using Domain.Entities;
using Domain.IService;

namespace Application.Service
{
    public class ProductService : IProductService
    {
        private const string DUPLICATED_RECORD = "Theres already a product saved in Db";
        private const string RECORD_NOT_FOUND = "No product with name ";
        private readonly IProductDomainService productDomainService;
        private readonly ICategoryDomainService categoryDomainService;
        private readonly IMapper mapper;

        public ProductService(IProductDomainService productDomainService, IMapper mapper, ICategoryDomainService categoryDomainService)
        {
            this.productDomainService = productDomainService;
            this.mapper = mapper;
            this.categoryDomainService = categoryDomainService;
        }

        public async Task<ProductDTO> AddProduct(ProductDTO productDTO)
        {
            await CheckProductDuplicity(productDTO);
            var categoriesInDb = await CreateCategoriesList(productDTO.Categories);
            var productSaved = await productDomainService.Save(mapper.Map<Product>(productDTO));
            productSaved.CategoryProducts = GenerateCategoryProductsList(productSaved, categoriesInDb);
            return mapper.Map<ProductDTO>(await productDomainService.Update(productSaved));
        }

        private ICollection<CategoryProduct> GenerateCategoryProductsList(Product productSaved, ICollection<Category> categoriesInDb)
        {
            ICollection<CategoryProduct> categoryProducts = new List<CategoryProduct>();
            foreach (var category in categoriesInDb)
            {
                categoryProducts.Add(new CategoryProduct
                {
                    CategoryId = category.Id,
                    ProductId = productSaved.Id
                });
            }
            return categoryProducts;
        }

        private async Task CheckProductDuplicity(ProductDTO productDTO)
        {
            var prodInDb = await productDomainService.Find(p => p.Name == productDTO.Name);
            if (prodInDb != null)
            {
                throw new DuplicateRecordInDbException(DUPLICATED_RECORD);
            }
        }

        private async Task<ICollection<Category>> CreateCategoriesList(ICollection<string> categoriesDTO)
        {
            ICollection<Category> categories = new List<Category>();
            foreach (var category in categoriesDTO)
            {
                categories.Add(await CheckCategoryExistence(category));
            }
            return categories;
        }

        private async Task<Category> CheckCategoryExistence(string category)
        {
            var categoryInDb = await categoryDomainService.Find(c => c.Name == category);
            if (categoryInDb == null)
            {
                throw new NoRecordFoundInDbException("No category: " + category + " found in database");
            }
            return categoryInDb;
        }

        public async Task<ProductDTO> DeleteProduct(ProductDTO productDTO)
        {
            var productInDb = await GetProductIfExists(productDTO);
            return mapper.Map<ProductDTO>(await productDomainService.Delete(productInDb));
        }

        public async Task<ICollection<ProductDTO>> GetAllProducts()
        {
            return mapper.Map<List<ProductDTO>>(await productDomainService.FindAllProducts());
        }

        public async Task<ProductDTO> GetProductByName(string name)
        {
            return mapper.Map<ProductDTO>(await productDomainService.FindProductByName(name));
        }

        public async Task<ProductDTO> UpdateProduct(ProductDTO catDTO)
        {
            var productInDb = await GetProductIfExists(catDTO);
            var categoriesInDb = await CreateCategoriesList(catDTO.Categories);
            productInDb.CategoryProducts = GenerateCategoryProductsList(productInDb, categoriesInDb);
            MakeProductChanges(catDTO, productInDb);
            return mapper.Map<ProductDTO>(await productDomainService.Update(productInDb));

        }

        private static void MakeProductChanges(ProductDTO catDTO, Product productInDb)
        {
            productInDb.MinimunInventory = catDTO.MinimunInventory;
            productInDb.Price = catDTO.Price;
            productInDb.ProfitPercentage = catDTO.ProfitPercentage;
            productInDb.WholeSaleQuantity = catDTO.WholeSaleQuantity;
            productInDb.WholeSalePrice = catDTO.WholeSalePrice;
        }

        private async Task<Product> GetProductIfExists(ProductDTO catDTO)
        {
            var productInDb = await productDomainService.FindProductByName(catDTO.Name);
            if (productInDb == null)
            {
                throw new NoRecordFoundInDbException(RECORD_NOT_FOUND + catDTO.Name);
            }
            return productInDb;
        }
    }
}
