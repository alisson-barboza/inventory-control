﻿using System.ComponentModel.DataAnnotations;

namespace Application.Entities
{
    public class UpdateCategoryDTO
    {
        [Required(AllowEmptyStrings = false)]
        [StringLength(20)]
        [RegularExpression(@"^[a-zA-Z]+$")]
        public string OldName { get; set; }

        [Required(AllowEmptyStrings = false)]
        [StringLength(20)]
        [RegularExpression(@"^[a-zA-Z]+$")]
        public string NewName { get; set; }
    }
}
