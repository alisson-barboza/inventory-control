﻿using System.ComponentModel.DataAnnotations;

namespace Application.Entities
{
    public class CategoryDTO
    {
        [Required(AllowEmptyStrings = false)]
        [StringLength(20)]
        [RegularExpression(@"^[a-zA-Z]+$")]
        public string Name { get; set; }
    }
}
