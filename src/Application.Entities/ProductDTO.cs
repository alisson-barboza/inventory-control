﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Application.Entities
{
    public class ProductDTO
    {
        [Required]
        [RegularExpression(@"^[a-zA-Z ]+$")]
        public string Name { get; set; }

        [Required]
        [Range(1, int.MaxValue)]
        public int MinimunInventory { get; set; }

        [Required]
        public string BarCode { get; set; }

        [Required]
        [RegularExpression(@"(^\d{1,4}.\d{1,2})?$", ErrorMessage = "Invalid price")]
        public decimal Price { get; set; }

        [Required]
        [Range(1, int.MaxValue)]
        public int WholeSaleQuantity { get; set; }

        [Required]
        [RegularExpression(@"(^\d{1,4}.\d{1,2})?$", ErrorMessage = "Invalid price")]
        public decimal WholeSalePrice { get; set; }

        [Required]
        [RegularExpression(@"(^\d{1,3}.\d{1,2})?$", ErrorMessage = "Invalid price")]
        public decimal ProfitPercentage { get; set; }

        [Required]
        public ICollection<string> Categories { get; set; }
    }
}
