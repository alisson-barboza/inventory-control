﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Infrastructure.EntityFramework.Migrations
{
    public partial class AddProductTableAndRelationshipCategory : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "TB_PRODUCT",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CreationDate = table.Column<DateTime>(nullable: false, defaultValueSql: "getdate()"),
                    Name = table.Column<string>(nullable: true),
                    MinimunInventory = table.Column<int>(nullable: false),
                    BarCode = table.Column<string>(nullable: false),
                    Price = table.Column<decimal>(type: "numeric(6, 2)", nullable: false),
                    WholeSaleQuantity = table.Column<int>(nullable: false),
                    WholeSalePrice = table.Column<decimal>(type: "numeric(6, 2)", nullable: false),
                    ProfitPercentage = table.Column<decimal>(type: "numeric(5, 2)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TB_PRODUCT", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "TB_CATEGORY_PRODUCT",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CreationDate = table.Column<DateTime>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    CategoryId = table.Column<Guid>(nullable: false),
                    ProductId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TB_CATEGORY_PRODUCT", x => x.Id);
                    table.ForeignKey(
                        name: "FK_TB_CATEGORY_PRODUCT_TB_CATEGORY_CategoryId",
                        column: x => x.CategoryId,
                        principalTable: "TB_CATEGORY",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_TB_CATEGORY_PRODUCT_TB_PRODUCT_ProductId",
                        column: x => x.ProductId,
                        principalTable: "TB_PRODUCT",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_TB_CATEGORY_PRODUCT_CategoryId",
                table: "TB_CATEGORY_PRODUCT",
                column: "CategoryId");

            migrationBuilder.CreateIndex(
                name: "IX_TB_CATEGORY_PRODUCT_ProductId",
                table: "TB_CATEGORY_PRODUCT",
                column: "ProductId");

            migrationBuilder.CreateIndex(
                name: "IX_TB_PRODUCT_Name",
                table: "TB_PRODUCT",
                column: "Name",
                unique: true,
                filter: "[Name] IS NOT NULL");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "TB_CATEGORY_PRODUCT");

            migrationBuilder.DropTable(
                name: "TB_PRODUCT");
        }
    }
}
