﻿
using Domain.Entities;

using Infrastructure.EntityFramework.EntityMapper;

using Microsoft.EntityFrameworkCore;

namespace Infrastructure.EntityFramework.DatabaseContext
{
    public class InventoryControlContext : DbContext
    {

        public DbSet<Category> Categories { get; set; }

        public InventoryControlContext(DbContextOptions<InventoryControlContext> options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new CategoryMapper());
            modelBuilder.ApplyConfiguration(new ProductMapper());
        }
    }
}
