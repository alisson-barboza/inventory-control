﻿using Domain.Entities;

using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Infrastructure.EntityFramework.EntityMapper
{
    public class ProductMapper : IEntityTypeConfiguration<Product>
    {
        public void Configure(EntityTypeBuilder<Product> builder)
        {
            builder.ToTable("TB_PRODUCT");

            builder.HasKey(p => p.Id);
            builder.HasIndex(p => p.Name).IsUnique().HasFilter("[Name] IS NOT NULL");

            builder.Property(p => p.CreationDate).IsRequired(true).ValueGeneratedOnAdd().HasDefaultValueSql("getdate()");

            builder.Property(p => p.Price).IsRequired(true).HasColumnType("numeric(6, 2)");
            builder.Property(p => p.MinimunInventory).IsRequired(true);
            builder.Property(p => p.BarCode).IsRequired(true);
            builder.Property(p => p.ProfitPercentage).IsRequired(true).HasColumnType("numeric(5, 2)");
            builder.Property(p => p.WholeSalePrice).IsRequired(true).HasColumnType("numeric(6, 2)");
            builder.Property(p => p.WholeSaleQuantity).IsRequired(true);
        }
    }
}
