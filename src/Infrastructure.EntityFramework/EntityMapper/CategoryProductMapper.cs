﻿using Domain.Entities;

using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Infrastructure.EntityFramework.EntityMapper
{
    public class CategoryProductMapper
    {
        public void Configure(EntityTypeBuilder<CategoryProduct> builder)
        {
            builder.HasKey(t => new { t.CategoryId, t.ProductId });

            builder.HasOne(c => c.Category).WithMany(c => c.CategoryProducts).HasForeignKey(c => c.CategoryId).OnDelete(DeleteBehavior.SetNull);
            builder.HasOne(p => p.Product).WithMany(p => p.CategoryProducts).HasForeignKey(p => p.ProductId).OnDelete(DeleteBehavior.SetNull);
        }
    }
}
