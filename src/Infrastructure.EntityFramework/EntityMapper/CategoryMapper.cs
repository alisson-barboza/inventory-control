﻿
using Domain.Entities;

using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Infrastructure.EntityFramework.EntityMapper
{
    public class CategoryMapper : IEntityTypeConfiguration<Category>
    {
        public void Configure(EntityTypeBuilder<Category> builder)
        {
            builder.ToTable("TB_CATEGORY");

            builder.HasKey(c => c.Id);
            builder.HasIndex(p => p.Name).IsUnique().HasFilter("[Name] IS NOT NULL");

            builder.Property(p => p.CreationDate).IsRequired(true).ValueGeneratedOnAdd().HasDefaultValueSql("getdate()");
        }
    }
}
