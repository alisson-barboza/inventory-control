﻿using System.Collections.Generic;

namespace Domain.Entities
{
    public class Category : BaseEntity
    {
        public ICollection<CategoryProduct> CategoryProducts { get; set; }
    }
}
