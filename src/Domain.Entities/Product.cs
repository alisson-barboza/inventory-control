﻿using System.Collections.Generic;

namespace Domain.Entities
{
    public class Product : BaseEntity
    {
        public int MinimunInventory { get; set; }
        public string BarCode { get; set; }
        public decimal Price { get; set; }
        public int WholeSaleQuantity { get; set; }
        public decimal WholeSalePrice { get; set; }
        public decimal ProfitPercentage { get; set; }

        public ICollection<CategoryProduct> CategoryProducts { get; set; }
    }
}
