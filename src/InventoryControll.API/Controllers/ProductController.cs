﻿
using System.Threading.Tasks;

using Application.Entities;
using Application.IService;

using Microsoft.AspNetCore.Mvc;

namespace InventoryControll.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductController : ControllerBase
    {
        private readonly IProductService productService;
        public ProductController(IProductService productService)
        {
            this.productService = productService;
        }

        [HttpPost]
        [Route("new")]
        public async Task<IActionResult> AddProduct(ProductDTO productDTO)
        {
            return Created("", await productService.AddProduct(productDTO));
        }

        [HttpDelete]
        [Route("delete")]
        public async Task<IActionResult> DeleteProduct(ProductDTO productDTO)
        {
            return Ok(await productService.DeleteProduct(productDTO));
        }

        [HttpGet]
        [Route("all")]
        public async Task<IActionResult> GetAllProducts()
        {
            return Ok(await productService.GetAllProducts());
        }

        [HttpGet]
        [Route("get/{name}")]
        public async Task<IActionResult> GetProductByName([FromRoute] string name)
        {
            return Ok(await productService.GetProductByName(name));
        }

        [HttpPost]
        [Route("update")]
        public async Task<IActionResult> UpdateProduct(ProductDTO productDTO)
        {
            return Ok(await productService.UpdateProduct(productDTO));
        }
    }
}
