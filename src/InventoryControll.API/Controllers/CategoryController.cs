﻿using System.Threading.Tasks;

using Application.Entities;
using Application.IService;

using Microsoft.AspNetCore.Mvc;

namespace InventoryControll.API.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class CategoryController : ControllerBase
    {
        private readonly ICategoryService categoryService;

        public CategoryController(ICategoryService categoryService)
        {
            this.categoryService = categoryService;
        }

        [HttpPost]
        [Route("new")]
        public async Task<IActionResult> AddCategory(CategoryDTO catDTO)
        {
            return Created("", await categoryService.AddCategory(catDTO));
        }

        [HttpGet]
        [Route("get/{name}")]
        public async Task<IActionResult> GetCategoryByName([FromRoute] string name)
        {
            return Ok(await categoryService.GetCategoryByName(name));
        }

        [HttpGet]
        [Route("all")]
        public async Task<IActionResult> GetAllCategories()
        {
            return Ok(await categoryService.GetAllCategories());
        }

        [HttpPut]
        [Route("update")]
        public async Task<IActionResult> UpdateCategory(UpdateCategoryDTO updateCategoryDTO)
        {
            return Ok(await categoryService.UpdateCategory(updateCategoryDTO));
        }

        [HttpDelete]
        [Route("delete")]
        public async Task<IActionResult> DeleteCategory(CategoryDTO categoryDTO)
        {
            await categoryService.DeleteCategory(categoryDTO);
            return NoContent();
        }
    }
}
