﻿
using Infrastructure.EntityFramework.DatabaseContext;

using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Cross.Cutting
{
    public static class DatabaseConnection
    {
        public static IServiceCollection Connect(this IServiceCollection service, IConfiguration configuration)
        {
            service.AddDbContext<InventoryControlContext>(opt => opt.
                UseSqlServer(configuration.GetConnectionString("DefaultConnection")));
            return service;
        }

    }
}
