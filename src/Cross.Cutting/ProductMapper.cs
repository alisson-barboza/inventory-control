﻿using Application.Entities;

using AutoMapper;

using Domain.Entities;

namespace Cross.Cutting
{
    public class ProductMapper : Profile
    {
        public ProductMapper()
        {
            CreateMap<ProductDTO, Product>()
                .ForMember(m => m.Id, opt => opt.Ignore())
                .ForMember(m => m.CreationDate, opt => opt.Ignore())
                .ForMember(m => m.CategoryProducts, opt => opt.Ignore());

            CreateMap<Product, ProductDTO>()
                .ForMember(m => m.Categories, opt => opt.ConvertUsing(new ProductCategoryFormatter(), p => p.CategoryProducts));

        }
    }
}
