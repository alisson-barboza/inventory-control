﻿using Application.Entities;

using AutoMapper;

using Domain.Entities;

namespace Cross.Cutting
{
    public class CategoryMapper : Profile
    {
        public CategoryMapper()
        {
            CreateMap<CategoryDTO, Category>()
                .ForMember(m => m.Id, opt => opt.Ignore())
                .ForMember(m => m.CreationDate, opt => opt.Ignore())
                .ForMember(m => m.CategoryProducts, opt => opt.Ignore());

            CreateMap<Category, CategoryDTO>();

        }
    }
}
