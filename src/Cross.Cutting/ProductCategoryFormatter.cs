﻿using System.Collections.Generic;

using AutoMapper;

using Domain.Entities;

namespace Cross.Cutting
{
    public class ProductCategoryFormatter : IValueConverter<ICollection<CategoryProduct>, ICollection<string>>
    {
        public ICollection<string> Convert(ICollection<CategoryProduct> sourceMember, ResolutionContext context)
        {
            ICollection<string> categories = new List<string>();
            if (sourceMember == null)
            {
                return categories;
            }
            foreach (var categoryProduct in sourceMember)
            {
                categories.Add(categoryProduct.Category.Name);
            }
            return categories;
        }
    }
}
