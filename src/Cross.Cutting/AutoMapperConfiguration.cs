﻿using AutoMapper;

namespace Cross.Cutting
{
    public class AutoMapperConfiguration
    {
        public static MapperConfiguration ConfigureMappers()
        {
            return new MapperConfiguration(m =>
            {
                m.AddProfile(new CategoryMapper());
                m.AddProfile(new ProductMapper());
            });
        }
    }
}
