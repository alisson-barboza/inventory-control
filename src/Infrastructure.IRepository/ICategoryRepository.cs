﻿using Domain.Entities;

namespace Infrastructure.IRepository
{
    public interface ICategoryRepository : IBaseRepository<Category>
    {
    }
}
