﻿using System.Collections.Generic;
using System.Threading.Tasks;

using Domain.Entities;

namespace Infrastructure.IRepository
{
    public interface IProductRepository : IBaseRepository<Product>
    {
        Task<ICollection<Product>> FindAllProducts();
        Task<Product> FindProductByName(string name);
    }
}
