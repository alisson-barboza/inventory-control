﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

using Domain.Entities;

namespace Infrastructure.IRepository
{
    public interface IBaseRepository<T> where T : BaseEntity
    {
        Task<T> Save(T entity);
        Task<T> Update(T entity);
        Task<T> Delete(T entity);
        Task<ICollection<T>> FindAll(params Expression<Func<T, object>>[] includes);
        Task<T> Find(Expression<Func<T, bool>> expression);
        Task<ICollection<T>> FindMany(Expression<Func<T, bool>> expression);
    }
}
