﻿using System.Collections.Generic;
using System.Threading.Tasks;

using Application.Entities;

namespace Application.IService
{
    public interface IProductService
    {
        Task<ProductDTO> AddProduct(ProductDTO catDTO);
        Task<ProductDTO> UpdateProduct(ProductDTO catDTO);
        Task<ProductDTO> DeleteProduct(ProductDTO catDTO);
        Task<ICollection<ProductDTO>> GetAllProducts();
        Task<ProductDTO> GetProductByName(string name);
    }
}
