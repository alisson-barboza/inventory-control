﻿using System.Collections.Generic;
using System.Threading.Tasks;

using Application.Entities;

namespace Application.IService
{
    public interface ICategoryService
    {
        Task<CategoryDTO> AddCategory(CategoryDTO catDTO);
        Task<CategoryDTO> UpdateCategory(UpdateCategoryDTO catDTO);
        Task<CategoryDTO> DeleteCategory(CategoryDTO catDTO);
        Task<ICollection<CategoryDTO>> GetAllCategories();
        Task<CategoryDTO> GetCategoryByName(string name);
    }
}
