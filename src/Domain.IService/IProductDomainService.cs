﻿using System.Collections.Generic;
using System.Threading.Tasks;

using Domain.Entities;

namespace Domain.IService
{
    public interface IProductDomainService : IBaseDomainService<Product>
    {
        Task<ICollection<Product>> FindAllProducts();
        Task<Product> FindProductByName(string name);
    }
}
