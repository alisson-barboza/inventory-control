﻿using Domain.Entities;

namespace Domain.IService
{
    public interface ICategoryDomainService : IBaseDomainService<Category>
    {
    }
}
