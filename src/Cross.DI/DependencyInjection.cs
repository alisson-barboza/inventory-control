﻿
using Application.IService;
using Application.Service;

using Domain.IService;
using Domain.Service;

using Infrastructure.IRepository;
using Infrastructure.Repository;

using Microsoft.Extensions.DependencyInjection;

namespace Cross.DI
{
    public static class DependencyInjection
    {
        public static void Inject(this IServiceCollection services)
        {
            // Application
            services.AddTransient<ICategoryService, CategoryService>();
            services.AddTransient<IProductService, ProductService>();

            // Domain
            services.AddScoped(typeof(IBaseDomainService<>), typeof(BaseDomainService<>));
            services.AddTransient<ICategoryDomainService, CategoryDomainService>();
            services.AddTransient<IProductDomainService, ProductDomainService>();

            // Repository
            services.AddScoped(typeof(IBaseRepository<>), typeof(BaseRepository<>));
            services.AddTransient<ICategoryRepository, CategoryRepository>();
            services.AddTransient<IProductRepository, ProductRepository>();
        }
    }
}
