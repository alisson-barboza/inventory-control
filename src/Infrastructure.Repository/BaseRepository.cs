﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

using Domain.Entities;

using Infrastructure.EntityFramework.DatabaseContext;
using Infrastructure.IRepository;

using Microsoft.EntityFrameworkCore;

namespace Infrastructure.Repository
{
    public class BaseRepository<T> : IBaseRepository<T> where T : BaseEntity
    {
        protected DbSet<T> entities;
        protected InventoryControlContext dbContext;

        public BaseRepository(InventoryControlContext dbContext)
        {
            this.dbContext = dbContext;
            entities = dbContext.Set<T>();
        }

        public async Task<T> Delete(T entity)
        {
            entities.Remove(entity);
            await dbContext.SaveChangesAsync();
            return entity;
        }

        public async Task<T> Find(Expression<Func<T, bool>> expression)
        {
            return await entities.Where(expression).FirstOrDefaultAsync();
        }

        public async Task<ICollection<T>> FindAll(params Expression<Func<T, object>>[] includes)
        {
            IQueryable<T> query = entities;
            foreach (var include in includes)
            {
                query = query.Include(include);
            }
            return await query.ToListAsync();
        }

        public async Task<ICollection<T>> FindMany(Expression<Func<T, bool>> expression)
        {
            return await entities.Where(expression).ToListAsync();
        }

        public async Task<T> Save(T entity)
        {
            var savedEntity = entities.Add(entity).Entity;
            await dbContext.SaveChangesAsync();
            return savedEntity;
        }

        public async Task<T> Update(T entity)
        {
            dbContext.Attach(entity);
            dbContext.Entry(entity).State = EntityState.Modified;

            await dbContext.SaveChangesAsync();
            return entity;
        }
    }
}
