﻿
using Domain.Entities;

using Infrastructure.EntityFramework.DatabaseContext;
using Infrastructure.IRepository;

namespace Infrastructure.Repository
{
    public class CategoryRepository : BaseRepository<Category>, ICategoryRepository
    {
        public CategoryRepository(InventoryControlContext dbContext) : base(dbContext)
        {
        }
    }
}
