﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Domain.Entities;

using Infrastructure.EntityFramework.DatabaseContext;
using Infrastructure.IRepository;

using Microsoft.EntityFrameworkCore;

namespace Infrastructure.Repository
{
    public class ProductRepository : BaseRepository<Product>, IProductRepository
    {

        public ProductRepository(InventoryControlContext dbContext) : base(dbContext)
        {
        }

        public async Task<ICollection<Product>> FindAllProducts()
        {
            return await entities.Include(p => p.CategoryProducts).ThenInclude(c => c.Category).ToListAsync();
        }

        public async Task<Product> FindProductByName(string name)
        {
            return await entities.Where(p => p.Name == name).Include(p => p.CategoryProducts).ThenInclude(c => c.Category).FirstOrDefaultAsync();
        }
    }
}
